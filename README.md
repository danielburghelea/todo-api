## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- composer install
- php artisan serve

## Request example

- Create a ToDo
```php
$client = new Client();
$headers = [
'Cookie' => 'laravel_session=VxPWVbq7V5GDBOI6qReq81xo0Iw2NBSBJBPvVFeM'
];
$options = [
'multipart' => [
[
'name' => 'title',
'contents' => 'test5'
],
[
'name' => 'body',
'contents' => 'message5'
]
]];
$request = new Request('POST', 'http://localhost:8000/api/todos', $headers);
$res = $client->sendAsync($request, $options)->wait();
echo $res->getBody()
```

- Update a ToDo
```php
$client = new Client();
$headers = [
  'Cookie' => 'laravel_session=VxPWVbq7V5GDBOI6qReq81xo0Iw2NBSBJBPvVFeM'
];
$options = [
  'multipart' => [
    [
      'name' => 'title',
      'contents' => 'test4'
    ],
    [
      'name' => 'body',
      'contents' => 'message4'
    ]
]];
$request = new Request('PUT', 'http://localhost:8000/api/todos/1?UUID=8617fb59-bcfa-459c-804b-9cc91bfdc60e&title=test 002&body=body 002&isCompleted=1', $headers);
$res = $client->sendAsync($request, $options)->wait();
echo $res->getBody();
```

- Get all ToDos
```php
$client = new Client();
$headers = [
  'Cookie' => 'laravel_session=VxPWVbq7V5GDBOI6qReq81xo0Iw2NBSBJBPvVFeM'
];
$request = new Request('GET', 'http://localhost:8000/api/todos', $headers);
$res = $client->sendAsync($request)->wait();
echo $res->getBody();
```

- Delete a ToDo
```php
$client = new Client();
$headers = [
  'Cookie' => 'laravel_session=VxPWVbq7V5GDBOI6qReq81xo0Iw2NBSBJBPvVFeM'
];
$request = new Request('DELETE', 'http://localhost:8000/api/todos/cc2650b0-d2f3-4ecd-bd66-37a12dcb37ea', $headers);
$res = $client->sendAsync($request)->wait();
echo $res->getBody();
```
