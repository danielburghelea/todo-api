<?php

namespace App\Models;

use App\DTO\TodoDTO;
use Ramsey\Uuid\Uuid;

class Todo
{
    public static function getAll(): array
    {
        return session('todos', []);
    }

    public static function store(array $request): TodoDTO
    {
        $toDo = new TodoDTO(Uuid::uuid4(), $request["title"], $request["body"], false);

        session()->push('todos', $toDo->toArray());

        return $toDo;
    }

    public static function update(array $request): TodoDTO
    {
        $toDoList = static::getAll();
        $toDoUpdateDTO = new TodoDTO($request["UUID"], $request["title"], $request["body"], $request["isCompleted"]);

        $updatedTodos = array_map(function ($todo) use ($toDoUpdateDTO) {
            if ($todo['UUID'] === $toDoUpdateDTO->UUID) {
                $todo = $toDoUpdateDTO->toArray();
            }

            return $todo;
        }, $toDoList);

        session()->put('todos', $updatedTodos);

        return $toDoUpdateDTO;
    }

    public static function delete(string $UUID): void
    {
        $toDoList = static::getAll();

        $filteredTodos = array_filter($toDoList, function ($todo) use ($UUID) {
            return $todo['UUID'] !== $UUID;
        });

        session()->put('todos', array_values($filteredTodos));
    }
}
