<?php

namespace App\DTO;

use Illuminate\Contracts\Support\Arrayable;

class TodoDTO implements Arrayable
{
    public string $UUID;
    public string $title;
    public string $body;
    public bool $isCompleted;

    public function __construct(string $UUID, string $title, string $body, bool $isCompleted)
    {
        $this->UUID = $UUID;
        $this->title = $title;
        $this->body = $body;
        $this->isCompleted = $isCompleted;
    }

    public function toArray(): array
    {
        return [
            'UUID' => $this->UUID,
            'title' => $this->title,
            'body' => $this->body,
            'isCompleted' => $this->isCompleted,
        ];
    }
}