<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreToDoPostRequest;
use App\Http\Requests\UpdateToDoPutRequest;
use App\Models\Todo;

class TodoController extends Controller
{
    public function index()
    {
        return Todo::getAll();
    }

    public function store(StoreToDoPostRequest $request)
    {
        $todo = Todo::store($request->only(['title', 'body']));
        return response()->json($todo, 201);
    }

    public function update(UpdateToDoPutRequest $request)
    {
        $todo = Todo::update($request->only(['UUID', 'title', 'body', 'isCompleted']));
        return response()->json($todo, 200);
    }

    public function destroy(string $uuid)
    {
        Todo::delete($uuid);
        return response()->json(null, 204);
    }
}
