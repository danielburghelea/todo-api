<?php

namespace App\Http\Requests;

class StoreToDoPostRequest extends ToDoRequest
{
    public function rules(): array
    {
        return [
            'title' => 'required|max:255',
            'body' => 'required',
        ];
    }
}
