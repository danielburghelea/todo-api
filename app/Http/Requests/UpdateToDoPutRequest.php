<?php

namespace App\Http\Requests;

class UpdateToDoPutRequest extends ToDoRequest
{
    public function rules()
    {
        return [
            'UUID' => 'required|uuid',
            'title' => 'required|string',
            'body' => 'required|string',
            'isCompleted' => 'required|boolean',
        ];
    }
}
