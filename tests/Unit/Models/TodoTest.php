<?php

namespace Tests\Unit\Models;

use App\DTO\TodoDTO;
use App\Models\Todo;
use Ramsey\Uuid\Uuid;
use Tests\TestCase;

class TodoTest extends TestCase
{
    /** @test */
    public function it_can_get_all_todos()
    {
        // Simulate some todos in the session
        $todosInSession = [
            [
                'UUID' => Uuid::uuid4()->toString(),
                'title' => 'Test 1',
                'body' => 'Message 1',
            ],
            [
                'UUID' => Uuid::uuid4()->toString(),
                'title' => 'Test 2',
                'body' => 'Message 2',
            ],
        ];
        session(['todos' => $todosInSession]);

        $todos = Todo::getAll();

        $this->assertEquals($todosInSession, $todos);
    }

    /** @test */
    public function it_can_store_a_todo()
    {
        $request = [
            'title' => 'Test 3',
            'body' => 'Message 3',
        ];

        $todo = Todo::store($request);

        $this->assertInstanceOf(TodoDTO::class, $todo);
        $this->assertNotNull($todo->UUID);
        $this->assertEquals($request['title'], $todo->title);
        $this->assertEquals($request['body'], $todo->body);

        $todosInSession = session('todos', []);
        $this->assertCount(1, $todosInSession);
        $this->assertEquals($todo->toArray(), $todosInSession[0]);
    }

    /** @test */
    public function it_can_update_a_todo()
    {
        // Simulate some todos in the session
        $todosInSession = [
            [
                'UUID' => 'dca68a5d-e69f-4f44-bc5b-e68e07cf4e52',
                'title' => 'Test 1',
                'body' => 'Message 1',
            ],
            [
                'UUID' => '185c6fe6-5eb5-4240-b76c-f0d1dd25dcdc',
                'title' => 'Test 2',
                'body' => 'Message 2',
            ],
        ];
        session(['todos' => $todosInSession]);

        $request = [
            'UUID' => 'dca68a5d-e69f-4f44-bc5b-e68e07cf4e52',
            'title' => 'Updated Title',
            'body' => 'Updated Message',
            'isCompleted' => true,
        ];

        $updatedTodo = Todo::update($request);

        $this->assertInstanceOf(TodoDTO::class, $updatedTodo);
        $this->assertEquals($request['UUID'], $updatedTodo->UUID);
        $this->assertEquals($request['title'], $updatedTodo->title);
        $this->assertEquals($request['body'], $updatedTodo->body);

        $updatedTodosInSession = session('todos', []);
        $this->assertCount(2, $updatedTodosInSession);
        $this->assertEquals($updatedTodo->toArray(), $updatedTodosInSession[0]);
    }

    /** @test */
    public function it_can_delete_a_todo()
    {
        // Simulate some todos in the session
        $todosInSession = [
            [
                'UUID' => 'dca68a5d-e69f-4f44-bc5b-e68e07cf4e52',
                'title' => 'Test 1',
                'body' => 'Message 1',
            ],
            [
                'UUID' => '185c6fe6-5eb5-4240-b76c-f0d1dd25dcdc',
                'title' => 'Test 2',
                'body' => 'Message 2',
            ],
        ];
        session(['todos' => $todosInSession]);

        $UUIDToDelete = 'dca68a5d-e69f-4f44-bc5b-e68e07cf4e52';

        Todo::delete($UUIDToDelete);

        $remainingTodosInSession = session('todos', []);
        $this->assertCount(1, $remainingTodosInSession);
        $this->assertEquals($todosInSession[1], $remainingTodosInSession[0]);
    }
}
